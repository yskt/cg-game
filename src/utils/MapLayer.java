package utils;

/* 
 * @author	yusuf
 * @version	1.0
 * @since	2021-11-03
 * */

import java.util.Scanner;
import java.io.InputStream;



public class MapLayer {
	private int column;
	private int row;
	private int data[][];

	public MapLayer(String path, int row, int column) {
		this.row = row;
		this.column = column;
		data = new int[row][column];
		init(path);
	}

	private void init(String path) {
		Scanner sc = null;

		InputStream in = getClass().getResourceAsStream(path);
		sc = new Scanner(in);
		sc.useDelimiter(",");

		for (int r = 0; r < row; ++r) {
			for (int c = 0; c < column; ++c) {
				if (sc.hasNextInt()) {
					data[r][c] = sc.nextInt()+1;
				} 
				else if (sc.hasNextLine()){
					sc.nextLine();
				}
			}
		}

		sc.close();
	}

	public int getTile(int r, int c) {return data[r][c];}

	public int rowCount() {return row;}
	public int columnCount() {return column;}
	public int nonemptyCount() {
		int count = 0;
		for (int r = 0; r < row; ++r) {
			for (int c = 0; c < column; ++c) {
				if (data[r][c] != 0) {
					++count;
				}
			}
		}
		return count;
	}
	
	public void print() {
		for (int r = 0; r < row; ++r) {
			for (int c = 0; c < column; ++c) {
				System.out.print(data[r][c] + ", ");
			}
			System.out.println("");
		}		
	}
}






