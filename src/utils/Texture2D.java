package utils;

/* 
 * @author	yusuf
 * @version	1.0
 * @since	2021-11-05
 * */

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;


public class Texture2D {
	private BufferedImage texture;
	
	public Texture2D(String path) {
		try {
			texture = ImageIO.read(getClass().getResource(path));
		} catch (IOException e) {
			System.err.println("load error");
			e.printStackTrace();
		}
	}
	
	public BufferedImage get() {return texture;}

}
