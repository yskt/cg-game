package utils;

/*
 * @author	yusuf
 * @version	1.0
 * @since	2021-11-06
 * */

public class Animation {
	
	private int tileSize;
	private int column;
	private int speed;
	private boolean running;
	private int frameTick;
	protected Vector2D frame;
	
	public Animation (int tileSize, int column, int frameMillis, int ups) {
		this.column = column;
		this.tileSize = tileSize;
		this.speed = ups / (1000 / frameMillis);
		frame = new Vector2D(0, 0);
		running = true;
		frameTick = 0;
	}

	public int getTileSize() {return tileSize;}
	public int getSpeed() {return speed;}
	public void setSpeed(int frameMillis) {speed = speed / (1000 / frameMillis);}
	public void reset() {frame.x=0;}

	public Vector2D getAFrame() {
		if (running) {
			frameTick++;
			if (frameTick >= speed) {
				if (frame.x < column - 1) {
					frame.x++;
				} else {
					frame.x = 0;
				}
				frameTick = 0;
			}
		}
		return frame;
	}

}









