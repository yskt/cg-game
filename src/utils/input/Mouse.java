package utils.input;

/* 
 * @author	yusuf
 * @version	1.0
 * @since	2021-11-6
 * */

import java.awt.event.MouseEvent;
import java.awt.Rectangle;

import javax.swing.event.MouseInputListener;

public class Mouse implements MouseInputListener {
	
	private boolean left = false;
	private int x = 0;
	private int y = 0;
	
	public boolean rectClicked(Rectangle rect) {
		if (
				left && 
				x >= rect.x && 
				x <= rect.x + rect.width &&
				y >= rect.y && 
				y <= rect.y + rect.height) {
			return true;
		}
		return false;
	}

	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		x = e.getX();
		y = e.getY();
		left = true;
	}

	@Override
	public void mouseReleased(MouseEvent e) {left = false;}

	@Override
	public void mouseDragged(MouseEvent arg0) {}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		x = arg0.getX();
		y = arg0.getY();
	}

}
