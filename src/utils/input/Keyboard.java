package utils.input;

/* 
 * @author	yusuf
 * @version	1.0
 * @since	2021-06-16
 * */

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

	private boolean[] pressed;
	
	public Keyboard() {
		pressed = new boolean[255];
	}
	
	public boolean isPressed(int keyCode) {
		return pressed[keyCode];
	}
	
	public void setPressed(int keycode, boolean b) {
		pressed[keycode] = b;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		pressed[e.getKeyCode()] = true;
	}

	@Override
	public void keyReleased(KeyEvent e) {
		pressed[e.getKeyCode()] = false;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

}
