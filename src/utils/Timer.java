package utils;

/* 
 * @author	yusuf
 * @version	1.0
 * @since	2021-06-16
 * */

public class Timer {
	private long start;
	private double stop;
	private boolean running;

	public Timer() {
		start = System.currentTimeMillis();
		stop = 0.0;
		running = false;
		start();
	}

	public double getSecond() {
		long now = System.currentTimeMillis();
		if (!running) {
			return stop;
		}
		return stop + ((now - start) / 1000.0);
	}

	public void stop() {
		if (running) {
			stop = getSecond();
			running = false;
		}
	}

	public void start() {
		if (!running) {
			running = true;
			start = System.currentTimeMillis();
		}
	}
	
}
