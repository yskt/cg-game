package utils;

/*
 * @author	yusuf
 * @version	1.0
 * @since	2021-06-16
 * */

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.WindowConstants;



public abstract class Window extends JFrame {
	private static final long serialVersionUID = 5599294301963294538L;
	protected Canvas canvas;
	
	public Window(String title, int width, int height) throws HeadlessException {
		initWindow(title, width, height);
	}

	private void initWindow(String title, int width, int height) {
		// exit program on close
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		// set window title
		setTitle(title);
		// do not allow resize
		setResizable(false);

		this.canvas = new Canvas();
		// set canvas size
        canvas.setPreferredSize(new Dimension(width, height));
        // keyboard input does not work if this is true
        canvas.setFocusable(false);
		
		add(canvas);
        pack();
        
        // create BufferStrategy to prevent flickering
        canvas.createBufferStrategy(2);
        // center window to screen
        setLocationRelativeTo(null);
        // validate
        validate();
        // set as visible
        setVisible(true);
	}

	public Canvas getCanvas() {return canvas;}
	
	public void setIcon(BufferedImage icon) {setIconImage(icon);}
}
