package utils;

/* 
 * @author	yusuf, fatma
 * @version	1.0
 * @since	2021-06-16
 * */

public class Vector2D {
	public int x;
	public int y;

	public Vector2D(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public double length() {return Math.sqrt(x*x + y*y);}
	public void negateX() {x = -x;}
	public void negateY() {y = -y;}
	
	public void negate() {
		x = -x;
		y = -y;
	}
	
	public void set(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void zero() {
		x = 0;
		y = 0;
	}

	public int toRow() {
		if 		(x == 0 && y > 0)	{return 0;}
		else if (x > 0 && y > 0) 	{return 1;}
		else if (x > 0 && y == 0) 	{return 2;}
		else if (x > 0 && y < 0) 	{return 3;}
		else if (x == 0 && y < 0) 	{return 4;}
		else if (x < 0 && y < 0) 	{return 5;}
		else if (x < 0 && y == 0) 	{return 6;}
		else if (x < 0 && y > 0) 	{return 7;}
		else 						{return 0;}
	}

	public int distanceTo(Vector2D other) {
		return (int) Math.sqrt((this.x - other.x)*(this.x - other.x) + (this.y - other.y)*(this.y - other.y));
	}

	@Override
	public String toString() {
		return "Vector2 [x=" + x + ", y=" + y + "]";
	}
}









