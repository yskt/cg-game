package utils;

/* 
 * @author	yusuf
 * @version	1.0
 * @since	2021-06-16
 * */


public interface PlayerInput {
	public boolean keyUp();
	public boolean keyDown();
	public boolean keyLeft();
	public boolean keyRight();
}
