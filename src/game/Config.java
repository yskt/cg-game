package game;
/*
 * @author	yusuf, fatma
 * @version	1.0
 * @since	2021-06-16
 * */

public class Config {
	public static final String TITLE = "CG Game";
	public static boolean DEBUG = false;
	public static final int UPS = 30;
	public static final int FPS = 90;
	public static final int UNIT = 12;//6;//12;
	public static final int TILE_SIZE = UNIT*4*2;
	public static final int NUMBER_OF_HORIZONTAL_TILES = 12;
	public static final int NUMBER_OF_VERTICAL_TILES = 8;
	public static final int WINDOW_WIDTH = TILE_SIZE*NUMBER_OF_HORIZONTAL_TILES;
	public static final int WINDOW_HEIGHT = TILE_SIZE*NUMBER_OF_VERTICAL_TILES;
}
