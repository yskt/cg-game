package game;

/* 
 * @author	yusuf
 * @version	1.0
 * @since	2021-06-16
 * */

import java.util.ArrayList;
import java.util.Random;
import java.awt.Rectangle;

import game.enemies.Ghost;
import utils.Vector2D;
import utils.input.Keyboard;
import utils.input.Mouse;
import game.enemies.Bat;
import game.enemies.Enemy;


public class Game {
	private Keyboard input = new Keyboard();
	private Mouse mouseInput = new Mouse();
	private PlayerInput playerInput;		
	protected GameWindow gameWindow;
	protected TileMap tileMap;
	protected Player player = null;
	protected ArrayList<Rectangle> collisionTiles;
	private ArrayList<GameObject> gameObjects;
	private Random rand = new Random();
	private Rectangle finish = new Rectangle(2248, 397, 200, 200);
	protected GameState state = GameState.MENU;
	protected Menu menu = new Menu();
	protected GameOver gameOver = new GameOver();
	protected boolean win;
	private int ghostCount;
	private int batCount;

	public Game() {
		playerInput = new PlayerInput(input);
		gameWindow = new GameWindow(Config.TITLE, Config.WINDOW_WIDTH, Config.WINDOW_HEIGHT, input, mouseInput);
		//
		tileMap = new TileMap();
		collisionTiles = new ArrayList<Rectangle>(tileMap.nonemptyCount("collision"));
		for (int r = 0; r < tileMap.rowCount("collision"); ++r) {
			for (int c = 0; c < tileMap.columnCount("collision"); ++c) {
				if (tileMap.getTile("collision", r, c) == 1) {
					collisionTiles.add(new Rectangle(
							c*Config.TILE_SIZE,
							r*Config.TILE_SIZE,
							Config.TILE_SIZE,
							Config.TILE_SIZE));
				}
			}	
		}
		player = new Player(600, 900, Config.UNIT, playerInput);
		gameObjects = new ArrayList<GameObject>();
		gameObjects.add(player);
		batCount = 3;
		ghostCount = 10;
		for (int i = 0; i < batCount; ++i) {
			int r = 1500+rand.nextInt(500);
			gameObjects.add(new Bat(r, r,  2 + 2*rand.nextInt(4)));
		}
		for (int i = 0; i < ghostCount; ++i) {
			int r = 1500+rand.nextInt(500);
			gameObjects.add(new Ghost(r, r,  2 + 2*rand.nextInt(4)));
		}
	}
	
	

	public ArrayList<GameObject> getGameObjects() {return gameObjects;}
	public void render() {gameWindow.render(this);}

	public void update() {
		// exit
		if (playerInput.keyExit() || mouseInput.rectClicked(menu.get("exit"))) {System.exit(0);}

		if (playerInput.keyPlay()) {
			state = GameState.RUNNING;
		}
		else if (playerInput.keyMenu()) {
			state = GameState.MENU;
		}

		if (state == GameState.RUNNING) {updateGame();}

		if (mouseInput.rectClicked(menu.get("play"))) {state = GameState.RUNNING;}

		if (mouseInput.rectClicked(menu.get("pause"))) {state = GameState.MENU;}
	}
	
	public void reset() {
		player = null;
		player = new Player(600, 900, Config.UNIT, playerInput);
		gameObjects = null;
		gameObjects = new ArrayList<GameObject>();
		gameObjects.add(player);
		
		for (int i = 0; i < batCount; ++i) {
			int r = 1500+rand.nextInt(500);
			gameObjects.add(new Bat(r, r,  2 + 2*rand.nextInt(4)));
		}
		for (int i = 0; i < ghostCount; ++i) {
			int r = 1500+rand.nextInt(500);
			gameObjects.add(new Ghost(r, r,  2 + 2*rand.nextInt(4)));
		}
	}

	public Vector2D getFocusPoint() {
		return new Vector2D(
				player.position.x + player.size.x/2, 
				player.position.y + player.size.y/2);
	}

	private void updateGame() {
		for (int i = 0; i < gameObjects.size(); ++i) {
			GameObject g = gameObjects.get(i);
			g.update();
			
			// horizontal movement
			g.moveH();
			g.followX(player);
			// horizontal tile collision
			if (g == player) {
				for (int t = 0; t < collisionTiles.size(); ++t) {
					if (g.checkCollisionRectangle(collisionTiles.get(t))) {
						if (g.direction.x > 0) {
							g.position.x = -g.collider.x + collisionTiles.get(t).x - g.collider.width;
						}
						if (g.direction.x < 0) {
							g.position.x = -g.collider.x + collisionTiles.get(t).x + collisionTiles.get(t).width;
						}
					}
				}	
			}
			// vertical movement
			g.moveV();
			g.followY(player);
			// vertical tile collision
			if (g == player) {
				for (int t = 0; t < collisionTiles.size(); ++t) {
					if (g.checkCollisionRectangle(collisionTiles.get(t))) {
						if (g.direction.y > 0) {
							g.position.y = -g.collider.y + collisionTiles.get(t).y - g.collider.height;
						}
						if (g.direction.y < 0) {
							g.position.y = -g.collider.y + collisionTiles.get(t).y + collisionTiles.get(t).height;
						}
					}
				}	
			}

			// update game object lives
			for (int j = 0; j < gameObjects.size(); ++j) {
				GameObject g2 = gameObjects.get(j);
				if (g2 != player && player.checkCollision2(g2.position, g2.collider)) {

					if ((player.state == GameObjState.ATTACK) && gameObjects.get(j) instanceof Enemy) {
						g2.position.x += player.direction.x * Config.TILE_SIZE;
						g2.position.y += player.direction.y * Config.TILE_SIZE;
						g2.lives--;
						if (g2.lives<=0) {gameObjects.remove(g2);}
					} 
				} 
				if (g2 != player && player.checkCollision(g2.position, g2.collider)) {
					player.lives--;
					if (player.lives<=0) {
						win = false;
						state = GameState.GAME_OVER;
						reset();
					}
					break;
				}
			}

			//
			if (player.checkCollisionRectangle(finish)) {
				win = true;
				state=GameState.GAME_OVER;
				reset();
			}

		}
	}



}
