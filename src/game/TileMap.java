package game;

/* 
 * @author	yusuf
 * @version	1.0
 * @since	2021-11-03
 * */

import java.util.HashMap;
import java.util.Map;

import utils.MapLayer;

public class TileMap {
	private Map<String, MapLayer> tileMap;
	
	public TileMap() {
		tileMap = new HashMap<String, MapLayer>();
		tileMap.put("ground",  new MapLayer("/map/data/ground.csv", 30, 31));
		tileMap.put("under",  new MapLayer("/map/data/under.csv", 30, 31));
		tileMap.put("above",  new MapLayer("/map/data/above.csv", 30, 31));
		tileMap.put("collision",  new MapLayer("/map/data/collision.csv", 30, 31));
	}
	
	public int getTile(String name, int r, int c) {return tileMap.get(name).getTile(r, c);}
	public int rowCount(String name) {return tileMap.get(name).rowCount();}
	public int columnCount(String name) {return tileMap.get(name).columnCount();}
	public void print(String name) {tileMap.get(name).print();}
	public int nonemptyCount(String name) {return tileMap.get(name).nonemptyCount();}
	
	
}
