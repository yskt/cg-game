package game;

/* 
 * @author	yusuf, fatma
 * @version	1.0
 * @since	2021-06-16
 * */

import java.util.Map;
import java.awt.Rectangle;

import utils.Animation;
import utils.Vector2D;


public abstract class GameObject {
	protected int speed;
	protected Vector2D position;
	protected Vector2D size;
	protected Vector2D direction;
	protected Rectangle collider;
	protected Rectangle hitBox;
	protected boolean colliding;
	protected boolean moving;
	protected GameObjState state; 
	protected Map<String, Animation> animations;
	protected String currentAnimation;
	protected Vector2D currentFrame;
	protected int lives;

	public GameObject(int x, int y, int width, int height, int speed) {
		this.speed = speed;
		position = new Vector2D(x, y);
		size = new Vector2D(width, height);
		direction = new Vector2D(0, 1);
		colliding = false;
		moving = false;
		state = GameObjState.IDLE;
		currentFrame = new Vector2D(0, 0);
		lives = 10;
	}

	public abstract void update();
	public Vector2D getDirection() {return direction;}
	public GameObjState getState() {return state;}
	public void setDirection(Vector2D vec) {this.direction = vec;}
	public void setState(GameObjState state) {this.state = state;}
	public void resetDirection() {direction.zero();}
	public Vector2D getPosition() {return position;}
	public Vector2D getSize() {return size;}
	public Rectangle getCollider() {return collider;}
	public int getAnimationTileSize() {return animations.get(currentAnimation).getTileSize();}

	public void setColliding(boolean colliding) {
		this.colliding = colliding;
	}

	public void followX(Player target) {}
	public void followY(Player target) {}
	
	public void moveH() {
		if (moving) {
			position.x += speed*direction.x;
			if (direction.x > 0) {
				hitBox.x = size.x/2;
			} else if (direction.x < 0) {
				hitBox.x = 0;
			} else {
				hitBox.x = size.x/2 - hitBox.width/2;
			}
		}
	}
	
	public void moveV() {
		if (moving) {
			position.y += speed*direction.y;
			if (direction.y > 0) {
				hitBox.y = size.y/2;
			} else if (direction.y < 0) {
				hitBox.y = 0;
			} else {
				hitBox.y = size.y/2 - hitBox.height/2;
			}
		}
	}

	public boolean checkCollisionRectangle(Rectangle col) {
		return
				position.x + collider.x < col.x + col.width &&
				position.x + collider.x + collider.width > col.x &&
				position.y + collider.y < col.y + col.height &&
				position.y + collider.y + collider.height > + col.y;
	}
	
	public boolean checkCollision(Vector2D pos, Rectangle col) {
		return
				position.x + collider.x < pos.x + col.x + col.width &&
				position.x + collider.x + collider.width > pos.x + col.x &&
				position.y + collider.y < pos.y + col.y + col.height &&
				position.y + collider.y + collider.height > pos.y + col.y;
	}
	
	public boolean checkCollision2(Vector2D pos, Rectangle col) {
		return
				position.x + hitBox.x < pos.x + col.x + col.width &&
				position.x + hitBox.x + hitBox.width > pos.x + col.x &&
				position.y + hitBox.y < pos.y + col.y + col.height &&
				position.y + hitBox.y + hitBox.height > pos.y + col.y;
	}


}
