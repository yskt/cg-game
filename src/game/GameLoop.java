package game;

/*
 * based on https://stackoverflow.com/a/23706266
 * @author	yusuf
 * @version	1.0
 * @since	2021-06-16
 * */

public class GameLoop extends utils.GameLoop {
	private Game game;

	
	public GameLoop(int UPS, int FPS, boolean running) {
		super(UPS, FPS, running);
		game = new Game();
	}
	
	@Override
	public void run() {
		long initialTime = System.currentTimeMillis();
		final double timeU = 1000 / UPS;
		final double timeF = 1000 / FPS;
		double deltaU = 0, deltaF = 0;
		long timer = System.currentTimeMillis();
		int frames = 0;
		int ticks = 0;

		while (running) {

			long currentTime = System.currentTimeMillis();
			deltaU += (currentTime - initialTime) / timeU;
			deltaF += (currentTime - initialTime) / timeF;
			initialTime = currentTime;

			if (deltaU >= 1) {
				update();
				ticks++;
				deltaU--;
			}

			if (deltaF >= 1) {
				render();
				frames++;
				deltaF--;
			}

			if (System.currentTimeMillis() - timer > 1000) {
				if (Config.DEBUG) {
					game.gameWindow.setTitle(Config.TITLE + "        " + "UPS: " + ticks + " FPS: " + frames);
//					System.out.println(String.format("UPS: %s, FPS: %s", ticks, frames));
	            }
	            frames = 0;
	            ticks = 0;
				timer += 1000;
			}
		}
	}

	@Override
	public void update() {
		game.update();
	}

	@Override
	public void render() {
		game.render();
	}

}










