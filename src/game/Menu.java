package game;

/*
 * @author	yusuf
 * @version	1.0
 * @since	2021-11-05
 * */

import java.util.HashMap;
import java.util.Map;
import java.awt.Rectangle;

public class Menu {
	
	protected Map<String, Rectangle> options;
	
	public Menu() {
		int w = Config.TILE_SIZE*3;
		int h = Config.TILE_SIZE*3/2;
		options = new HashMap<String, Rectangle>();
		options.put("play", new Rectangle(
				Config.WINDOW_WIDTH/2 - w/2, 
				Config.WINDOW_HEIGHT/2 - h/2 - h/2, 
				w, 
				h));
		options.put("exit", new Rectangle(
				Config.WINDOW_WIDTH/2 - w/2,
				Config.WINDOW_HEIGHT/2 - h/2 + h/2, 
				w,
				h));
		options.put("pause", new Rectangle(
				Config.WINDOW_WIDTH - w/2 - h/2/2, 
				h/2/2, 
				w/2, 
				h/2));
	}

	public Rectangle get(String name) {
		return options.get(name);
	}
	
	
}
