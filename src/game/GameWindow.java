package game;	

/*
 * @author	yusuf
 * @version	1.0
 * @since	2021-06-16
 * */

import java.awt.image.BufferStrategy;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.Cursor;
import java.awt.Point;

import utils.Vector2D;
import utils.input.Keyboard;
import utils.input.Mouse;

public class GameWindow extends utils.Window {

	private static final long serialVersionUID = -5384553046664110020L;
	private Textures textures;
	private Toolkit toolkit = Toolkit.getDefaultToolkit();
	private Cursor cursor = null;

	public GameWindow(String title, int width, int height, Keyboard input, Mouse mouseInput) {
		super(title, width, height);

		// mouse and key listener
		canvas.addMouseListener(mouseInput);
		addKeyListener(input);

		// load image resources
		textures = new Textures();


		// set cursor icon
		cursor = toolkit.createCustomCursor(
				textures.get("cursor"), 
				new Point(canvas.getX(), canvas.getY()), 
				"imgCursor");		
		canvas.setCursor(cursor);
		// set window icon
		this.setIcon(textures.get("icon"));

	}
	Graphics2D graphics;
	BufferStrategy bufferStrategy;
	public void render(Game game) {
		bufferStrategy = canvas.getBufferStrategy();
		graphics = (Graphics2D) bufferStrategy.getDrawGraphics();

		if (game.state == GameState.RUNNING) {
			// follow player
			beginCameraMode2D(game);

			// draw map layers
			drawMapLayer("ground", game);
			drawMapLayer("under", game);

			// draw game objects that are visible to the camera including player
			for (int i = 0; i < game.getGameObjects().size(); ++i) {
				GameObject g = game.getGameObjects().get(i);
				if (isVisibleToCamera(g.position.x, g.position.y, 1, game.getFocusPoint(), (Config.DEBUG) ? -Config.TILE_SIZE : Config.TILE_SIZE*2)) {//g.isInCamera(Config.WINDOW_WIDTH, Config.WINDOW_HEIGHT)) {					

					int row = g.currentFrame.y;
					int column = g.currentFrame.x;
					int size = g.getAnimationTileSize();

					// draw image
					graphics.drawImage(
							textures.get(g.currentAnimation), 
							g.position.x, g.position.y, g.position.x + g.size.x, g.position.y + g.size.y, // destination
							size*column, size*row, // source
							size + size*column, size + size*row,
							null);

					if (Config.DEBUG || false) {
						// draw frame
						graphics.setColor(new Color(0, 255, 0, 127));
						graphics.fillRect(g.position.x, g.position.y, g.size.x, g.size.y);
						// draw hitbox
						graphics.setColor(new Color(255, 0, 0, 127));
						graphics.fillRect(g.position.x + g.hitBox.x, g.position.y + g.hitBox.y, g.hitBox.width, g.hitBox.height);
						// draw collider
						graphics.setColor(new Color(0, 0, 255, 127));
						graphics.fillRect(g.position.x + g.collider.x, g.position.y + g.collider.y, g.collider.width, g.collider.height);
					}
				}
			}
			
			// draw player hp
			graphics.setColor(Color.RED);
			graphics.fillRect(
					game.player.position.x + game.player.size.x/2-game.player.lives/game.player.size.x/2*4, 
					game.player.position.y, 
					game.player.lives/game.player.size.x*4, 
					Config.TILE_SIZE/6);
			
			// this layer is drawn above the player
			drawMapLayer("above", game);
			
			// draw collision layer 
			if (Config.DEBUG) {
				drawMapLayer("collision", game);
			}

			// fixed elements
			endCameraMode2D(game);

			// draw puse button
			graphics.drawImage(
					textures.get("pause"),
					game.menu.get("pause").x,
					game.menu.get("pause").y,
					game.menu.get("pause").width,
					game.menu.get("pause").height,
					null);


		} else if (game.state == GameState.MENU) {
			// 
			beginCameraMode2D(game);


			drawMapLayer("ground", game);
			drawMapLayer("under", game);
			drawMapLayer("above", game);

			// draw fixed elements
			endCameraMode2D(game);

			graphics.setColor(new Color(0, 0, 0, 150));
			graphics.fillRect(0, 0, Config.WINDOW_WIDTH, Config.WINDOW_HEIGHT);


			// draw menu elements
			graphics.drawImage(
					textures.get("play"),
					game.menu.get("play").x,
					game.menu.get("play").y,
					game.menu.get("play").width,
					game.menu.get("play").height,
					null);

			graphics.drawImage(
					textures.get("exit"),
					game.menu.get("exit").x,
					game.menu.get("exit").y,
					game.menu.get("exit").width,
					game.menu.get("exit").height,
					null);
		} else if (game.state == GameState.GAME_OVER) {

			beginCameraMode2D(game);


			drawMapLayer("ground", game);
			drawMapLayer("under", game);
			drawMapLayer("above", game);

			// fixed elements
			endCameraMode2D(game);

			graphics.setColor(new Color(0, 0, 0, 150));
			graphics.fillRect(0, 0, Config.WINDOW_WIDTH, Config.WINDOW_HEIGHT);

			graphics.drawImage(
					textures.get("play"), game.menu.get("play").x, game.menu.get("play").y, game.menu.get("play").width, game.menu.get("play").height, null);

			graphics.drawImage(
					textures.get("exit"),
					game.menu.get("exit").x,
					game.menu.get("exit").y,
					game.menu.get("exit").width,
					game.menu.get("exit").height,
					null);
			if (game.win) {
				graphics.drawImage(
						textures.get("win"),
						game.gameOver.get("win").x,
						game.gameOver.get("win").y,
						game.gameOver.get("win").width,
						game.gameOver.get("win").height,
						null);
			} else if (!game.win) {
				graphics.drawImage(
						textures.get("lost"),
						game.gameOver.get("lost").x,
						game.gameOver.get("lost").y,
						game.gameOver.get("lost").width,
						game.gameOver.get("lost").height,
						null);
			}
		}


		graphics.dispose();
		bufferStrategy.show();
	}	

	public void beginCameraMode2D(Game game) {

		graphics.translate(
				-game.getFocusPoint().x+Config.WINDOW_WIDTH/2, 
				-game.getFocusPoint().y+Config.WINDOW_HEIGHT/2);
		// clear window
		graphics.setColor(new Color(150, 150, 150));
		graphics.fillRect(
				game.getFocusPoint().x-Config.WINDOW_WIDTH/2, 
				game.getFocusPoint().y-Config.WINDOW_HEIGHT/2, 
				Config.WINDOW_WIDTH, 
				Config.WINDOW_HEIGHT);
	}

	public void endCameraMode2D(Game game) {
		graphics.translate(
				game.getFocusPoint().x-Config.WINDOW_WIDTH/2, 
				game.getFocusPoint().y-Config.WINDOW_HEIGHT/2);
	}

	// draw only tiles which are visible to the camera
	public void drawMapLayer(String name, Game game) {
		for (int r = 0; r < 30; ++r) {
			for (int c = 0; c < 30; ++c) {
				int tile = game.tileMap.getTile(name, r, c);
				if (game.tileMap.getTile(name, r, c) != 0) {
					int t = Config.TILE_SIZE;
					int row = tile/25;
					int col = tile%25 - 1;
					if (isVisibleToCamera(c, r, t, game.getFocusPoint(), (Config.DEBUG) ? 0 : 2*t)) {
						graphics.drawImage(
								textures.get("map"),
								t*c, t*r, t*c+t, t*r+t, // destination rectangle
								16*col, 16*row, 16*col+16, 16*row+16, // source rectangle
								null);
					}
				}
			}
		}
	}

	// check if a tile is visible to the camera
	public boolean isVisibleToCamera(int x, int y, int size, Vector2D focus, int offset) {
		return 
				size*x > focus.x - Config.WINDOW_WIDTH/2 - offset && 
				size*(x + 1) < focus.x + Config.WINDOW_WIDTH/2 + offset &&
				size*y > focus.y - Config.WINDOW_HEIGHT/2 - offset &&
				size*(y + 1) < focus.y + Config.WINDOW_HEIGHT/2 + offset;
	}

}



