package game;

/* 
 * @author	yusuf
 * @version	1.0
 * @since	2021-11-05
 * */

import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Map;

public class GameOver {
protected Map<String, Rectangle> options;
	
	public GameOver() {
		options = new HashMap<String, Rectangle>();
		options.put("lost", new Rectangle(
				0, 
				Config.WINDOW_HEIGHT - Config.TILE_SIZE*4, 
				Config.TILE_SIZE*4, 
				Config.TILE_SIZE*4));
		options.put("win", new Rectangle(
				0, 
				Config.WINDOW_HEIGHT - Config.TILE_SIZE*4, 
				Config.TILE_SIZE*4, 
				Config.TILE_SIZE*4));
	}

	public Rectangle get(String name) {
		return options.get(name);
	}
	
}
