package game;

import java.awt.Rectangle;
import java.util.HashMap;
import utils.Animation;

/* 
 * @author	yusuf, fatma
 * @version	1.0
 * @since	2021-06-16
 * */

public class Player extends GameObject {
	private PlayerInput playerInput;
	private boolean attacking = false;
	private long start;
	private long attackTimer = 300;

	
	public Player(int x, int y, int speed, PlayerInput playerInput) {
		super(x, y, Config.TILE_SIZE*3, Config.TILE_SIZE*3, speed);
		int tileSize = Config.TILE_SIZE*3;
		collider = new Rectangle(tileSize/4+40, tileSize/2+35, tileSize/2-80, tileSize/4-20);
		hitBox = new Rectangle(0, 0, size.x/2, size.y/2);
		currentAnimation = "player-idle";
		animations = new HashMap<String, Animation>();
		animations.put("player-idle", new Animation(96, 4, 200, Config.UPS));
		animations.put("player-run", new Animation(96, 6, 100, Config.UPS));
		animations.put("player-attack", new Animation(96, 3, (int) attackTimer/3, Config.UPS));
		direction.set(0, 0);
		this.playerInput = playerInput;
		state = GameObjState.IDLE;
		start = 0;
		lives = Config.UPS*100*2;
	}

	public void processInput() {
		long now = System.currentTimeMillis();
		attacking=false;
		moving = false;

		if (!attacking&&(playerInput.keyDown() || playerInput.keyUp() || playerInput.keyRight() || playerInput.keyLeft())) {
			moving = true;
			direction.x = 0;
			direction.y = 0;
			state = GameObjState.RUN;
		}
		if (playerInput.keyUp()) {
			direction.y = -1;
		} 
		if (playerInput.keyRight()) {
			direction.x = 1;
		}
		if (playerInput.keyDown()) {
			direction.y = 1;
		} 
		if (playerInput.keyLeft()) {
			direction.x = -1;
		}
		if (playerInput.keyAttack(System.currentTimeMillis())) {
			start = now;
			attacking=true;
			moving = false;
			state = GameObjState.ATTACK;			
		}
		if (!moving && !attacking) {
			if (now - start >= attackTimer) {
				state = GameObjState.IDLE;
			}
		}

	}

	public void updateAnimation() {
		if (state == GameObjState.IDLE) {
			currentAnimation = "player-idle";
			animations.get("player-attack").reset();
		}		
		else if (state == GameObjState.RUN) {
			currentAnimation = "player-run";
			animations.get("player-attack").reset();
		}
		else if (state == GameObjState.ATTACK) {
			currentAnimation = "player-attack";	
		}
		
		currentFrame.x = animations.get(currentAnimation).getAFrame().x;
		currentFrame.y = direction.toRow();
	}

	@Override
	public void update() {
		processInput();
		updateAnimation();
	}
}
