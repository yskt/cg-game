package game;

/* 
 * @author	yusuf
 * @version	1.0
 * @since	2021-11-05
 * */

import java.util.HashMap;
import java.util.Map;
import java.awt.image.BufferedImage;

import utils.Texture2D;



public class Textures {
	private Map<String, Texture2D> textures;
	
	public Textures() {
		textures = new HashMap<String, Texture2D>();
		loadTextures();
	}
	
	private void loadTextures() {
		// map
		put("map", "/map/tileset-1-16.png");
		// player
		put("player-idle", "/player/idle.png");
		put("player-run", "/player/run.png");
		put("player-attack", "/player/attack.png");
		// enemies
		put("bat", "/npc/bat.png");
		put("ghost", "/npc/ghost.png");
		// ui
		put("icon", "/icon.png");
		put("play", "/ui/menu/play.png");
		put("exit", "/ui/menu/exit.png");
		put("pause", "/ui/menu/pause.png");
		put("cursor", "/ui/cursor.png");
		// game over
		put("win", "/player/win.png");
		put("lost", "/player/lost.png");
	}
	
	public void put(String name, String path) {	textures.put(name, new Texture2D(path));}
	public BufferedImage get(String name) {return textures.get(name).get();}
	
}
