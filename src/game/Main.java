package game;

/* 
 * @author	yusuf, fatma
 * @version	1.0
 * @since	2021-06-16
 * */

public class Main {
	private game.GameLoop gameLoop;

	public Main() {
		gameLoop = new game.GameLoop(Config.UPS, Config.FPS, true);
	}

	public static void main(String[] args) {
		if (args.length == 1 && (args[0].equalsIgnoreCase("true") || args[0].equalsIgnoreCase("false"))) {
			Config.DEBUG = Boolean.parseBoolean(args[0]);
		}
		new Main().gameLoop.run();
	}

}
