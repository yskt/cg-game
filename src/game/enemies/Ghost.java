package game.enemies;

/* 
 * @author	yusuf, fatma
 * @version	1.0
 * @since	2021-06-16
 * */

import java.awt.Rectangle;
import java.util.HashMap;

import utils.Animation;
import game.Config;

public class Ghost extends Enemy {
	public Ghost(int x, int y, int speed) {
		super(x, y, Config.TILE_SIZE*2, Config.TILE_SIZE*2, speed);
		collider =  new Rectangle(Config.TILE_SIZE/2, Config.TILE_SIZE/2, Config.TILE_SIZE, Config.TILE_SIZE);
		hitBox = new Rectangle(50, 5, 20, 10);

		moving = true;
		direction.x = 0;
		direction.y = 1;
		currentAnimation = "ghost";
		animations = new HashMap<String, Animation>();
		animations.put("ghost", new Animation(48, 4, 200, Config.UPS));

	}

	public void moveH() {}
	public void moveV() {}


	@Override
	public void update() {
		super.update();

		currentFrame.x = animations.get(currentAnimation).getAFrame().x;
		currentFrame.y = direction.toRow();



	}
}
