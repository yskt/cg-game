package game.enemies;

/* 
 * @author	yusuf, fatma
 * @version	1.0
 * @since	2021-06-16
 * */

import game.Config;
import game.GameObject;
import game.Player;



public class Enemy extends GameObject {
	public Enemy(int x, int y, int width, int height, int speed) {
		super(x, y, width, height, speed);
	}

	public boolean isEnemy() {
		return true;
	}

	public void followX(Player target) {
		int d = position.x-target.getPosition().x+target.getCollider().x + target.getCollider().width/2;
		if (d < Config.TILE_SIZE*7) {
			if (position.x+size.x/2 > target.getPosition().x + target.getSize().x/2 + speed) {
				position.x -= speed;
				direction.x = -1;
			} else if (position.x+size.x/2 < target.getPosition().x + target.getSize().x/2 - speed) {
				position.x += speed;
				direction.x = 1;
			} else {
				direction.x = 0;
			}
		}	
	}
	
	public void followY(Player target) {
		int d = position.y-target.getPosition().y+target.getCollider().y + target.getCollider().height/2;		
		if (d < Config.TILE_SIZE*7) {
			if (position.y+size.y/2 > target.getPosition().y + target.getSize().y/2 + speed) {
				position.y -= speed;
				direction.y = -1;
			} else if (position.y+size.y/2 < target.getPosition().y + target.getSize().y/2 - speed) {
				position.y += speed;
				direction.y = 1;
			} else {
				direction.y = 0;
			}

		}	
	}

	@Override
	public void update() {}

}
