package game;

/* 
 * @author	yusuf
 * @version	1.0
 * @since	2021-06-16
 * */

import java.awt.event.KeyEvent;

import utils.input.Keyboard;


public class PlayerInput implements utils.PlayerInput {
	private Keyboard input;
	private long st = System.currentTimeMillis();

	public PlayerInput(Keyboard input) {
		this.input = input; 
	}

	@Override
	public boolean keyUp() {
		return input.isPressed(KeyEvent.VK_UP) || input.isPressed(KeyEvent.VK_W);
	}

	@Override
	public boolean keyDown() {
		return input.isPressed(KeyEvent.VK_DOWN) || input.isPressed(KeyEvent.VK_S);
	}

	@Override
	public boolean keyLeft() {
		return input.isPressed(KeyEvent.VK_LEFT) || input.isPressed(KeyEvent.VK_A);
	}

	@Override
	public boolean keyRight() {
		return input.isPressed(KeyEvent.VK_RIGHT) || input.isPressed(KeyEvent.VK_D);
	}

	// prevents continuous usage of the attack key
	public boolean keyAttack(long t) {
		long aa = t - st;  

		if (aa > 300 && input.isPressed(KeyEvent.VK_X)) {
			st = System.currentTimeMillis();
			return true;
		}
		return false;
	}

	public boolean keyEscape() {
		return input.isPressed(KeyEvent.VK_ESCAPE);
	}

//	public boolean keyPlay() {
//		return input.isPressed(KeyEvent.VK_P);
//	}
	
	public boolean keyPlay() {
		boolean pressed = input.isPressed(KeyEvent.VK_P);
		input.setPressed(KeyEvent.VK_P, false);
		return pressed;
	}

	public boolean keyExit() {
		return input.isPressed(KeyEvent.VK_SHIFT) && input.isPressed(KeyEvent.VK_Q);
	}

	public boolean keyMenu() {
		return input.isPressed(KeyEvent.VK_M);
	}

	public boolean keyShift() {
		return input.isPressed(KeyEvent.VK_SHIFT);
	}

	public boolean keySpace() {
		return input.isPressed(KeyEvent.VK_SPACE);
	}

}
